// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TestSourcetreeGameMode.h"
#include "TestSourcetreeHUD.h"
#include "TestSourcetreeCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATestSourcetreeGameMode::ATestSourcetreeGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATestSourcetreeHUD::StaticClass();
}
