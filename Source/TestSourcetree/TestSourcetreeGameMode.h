// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestSourcetreeGameMode.generated.h"

UCLASS(minimalapi)
class ATestSourcetreeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATestSourcetreeGameMode();
};



